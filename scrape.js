import { Cursor, Rettiwt } from 'rettiwt-api'
import promptSync  from 'prompt-sync'
import { checkForKeys, getAndStoreKey, returnKey } from './helpers/auth.js'

const prompt = promptSync()

performAuth()

/**
 * This functions returns the list of retweets of a given list of tweet ids
 * 
 * @param {*} tweetIds The list of tweet ids
 */
async function getRetweetsOfTweetIds(tweetIds) {
    // Iterating over the list of tweet ids
    let cursor = ''
    
    for (let id of tweetIds) {
        const rettiwt = new Rettiwt({ apiKey: await returnKey() })

        rettiwt.tweet.retweeters(id, 100, cursor)
        .then(res => {
            console.log(res)
            cursor = res.next.value
            console.log('cursor ' + cursor)
        })
        .catch(err => {
            console.log(err);
        })
    }
}

async function performAuth(){
    let key = await checkForKeys()

    if (!key)
    {
        let email = prompt('Enter your X email address: ')
        let user = prompt('Enter your X user name: ')
        let password = prompt('Enter your X password: ')
        
        getAndStoreKey(email, user, password).then(() => {
            getRetweetsOfTweetIds(["1281582736176816129","1281582609936658433","1756841799505658108"]);
        })
    }
    else
    {
        getRetweetsOfTweetIds(["1281582736176816129","1281582609936658433","1756841799505658108"]);
    }
}

/**
 * Initializing a new Rettiwt instance using the API_KEY that has been stored as an environment variable.
 * You may also pass the API_KEY string here directly, however it's unsafe to do so since it can be misused if you ever happen to share the code with someone else.
 */

//const rettiwt = new Rettiwt({ apiKey: 'APIKEY'})

//const fs = require('fs');

//getRetweetsOfTweetIds(["1281582736176816129","1281582609936658433"]);

/*fs.readFile("/home/jemather/retweets/tweet_ids.csv", 'utf8', (err, data) => {
  if (err) {
    console.error(err);
    return;
  }
});*/