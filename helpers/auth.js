import keytar from 'keytar'
import { Rettiwt } from 'rettiwt-api'

export async function checkForKeys() 
{
    let key = await keytar.findPassword('twitter')
    return key == null ? false : true
}

export async function getAndStoreKey(email, user, pass) 
{
    const rettiwt = new Rettiwt();

    rettiwt.auth.login(email, user, pass)
    .then(apiKey => {
        try{
            keytar.setPassword('twitter', 'scraper', apiKey)
            return
        }
        catch(err)
        {
            console.log("Error while storing key")
            console.log(err)
        }
    })
    .catch((error) => {
        console.log('Well that\'s  embarrassing' + error )
        return
    })

}

export async function returnKey()
{
    return await keytar.findPassword('twitter')
}